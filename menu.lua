
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local suonoBottone

local function gotoGame() --passa al gioco
	audio.play( suonoBottone )
      composer.gotoScene( "game", { time=1900, effect="crossFade" } ) 
end
 
local function gotoHighScores()  --passa ai punteggi
	audio.play( suonoBottone )
     composer.gotoScene( "score", { time=900, effect="crossFade" } )
end

local function gotoTutorial()  --passa al tutorial
	audio.play( suonoBottone )
     composer.gotoScene( "tutorial", { time=900, effect="crossFade" } )
end

local function gotoCredits()  --passa ai credits
	audio.play( suonoBottone )
     composer.gotoScene( "credits", { time=900, effect="crossFade" } )
end


----------------------------------------------------------------------------------------------

local datiFoglioRicarica = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

	width = 433,--lunghezza del signolo frame
	height = 409,--altezza del singolo frame
	numFrames = 43,
	sheetContentWidth = 4763,--lunghezza totale function
	sheetContentheight = 1636,--idem
}


local datiRicarica = {
	{ 
		name = "pinguinoRicarica",
	 	start = 1,
		count = 43,
		time = 2000,
		loopCount = 1,
		loopDirection = "ended",
     }
}
		------------------------------------------------------CORSA-----------------------------------------------------------------------------

local pinguinoRicarica = graphics.newImageSheet ("move/ricarica.png", datiFoglioRicarica)
local animation = display.newSprite( pinguinoRicarica, datiRicarica )
animation.x = display.contentCenterX/2
animation.y = display.contentCenterY*1.2;
animation.xScale = 0.23
animation.yScale = 0.23


timer.performWithDelay(6000,animation:play())
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

--sfondo menu
local posi =  display.actualContentWidth / 2


local cielo = display.newImageRect(sceneGroup,"menu/cielo.png",display.actualContentWidth+3, display.actualContentHeight)
cielo.x = display.contentCenterX;
cielo.y = display.contentCenterY;

local montagne = display.newImageRect(sceneGroup,"menu/BG.png",display.actualContentWidth+3, display.actualContentHeight)
montagne.x = display.contentCenterX;
montagne.y = display.contentCenterY/0.95;

local alberi= display.newImageRect(sceneGroup,"menu/Middle.png",display.actualContentWidth+3, display.actualContentHeight)
alberi.x = display.contentCenterX;
alberi.y = display.contentCenterY/0.88; 

local pavi1 = display.newImageRect(sceneGroup,"menu/Ground_01.png",display.actualContentWidth+3, display.actualContentHeight)
pavi1.x = display.contentCenterX;
pavi1.y = display.contentCenterY;

local bordo = display.newImageRect(sceneGroup,"menu/Ground_02.png",display.actualContentWidth+3, display.actualContentHeight)
bordo.x = display.contentCenterX;
bordo.y = display.contentCenterY;


local sound = display.newImage(sceneGroup,"pulsanti/sound.png")
sound.x = display.contentCenterX + posi/1.2
sound.y = display.actualContentHeight * 0.2
sound.xScale = 0.08
sound.yScale = 0.08

local noSound = display.newImage(sceneGroup,"pulsanti/noSound.png")
noSound.x = display.contentCenterX + posi/1.2
noSound.y = display.actualContentHeight * 0.2
noSound.xScale = 0.08
noSound.yScale = 0.08
noSound.alpha = 0

 local function soundOff( )
    	if(noSound.alpha == 0) then
    	    noSound.alpha = 1;
    		sound.alpha = 0;
   end
end
    sound:addEventListener( "tap", soundOff )


 local function soundOn( ) 
   if(noSound.alpha == 1) then

          noSound.alpha = 0;
    		sound.alpha = 1;
 end
end


 noSound:addEventListener( "tap", soundOn )

suonoBottone = audio.loadSound( "suoni/suonoBottone.wav" )
audio.setVolume( 0.3, suonoBottone)

local score = display.newImage(sceneGroup,"pulsanti/score.png")
score.x = display.contentCenterX 
score.y = display.actualContentHeight * 0.66
score.xScale = 0.095
score.yScale = 0.095
noSound.alpha = 0;

local play = display.newImage(sceneGroup,"pulsanti/play.png")
play.x = display.contentCenterX + posi/1.5
play.y = display.actualContentHeight * 0.65
play.xScale = 0.089
play.yScale = 0.089


local credits = display.newImage(sceneGroup,"pulsanti/credits.png")
credits.x = display.contentCenterX 
credits.y = display.actualContentHeight * 0.85
credits.xScale = 0.06
credits.yScale = 0.06


local tutorial = display.newImage(sceneGroup,"pulsanti/tutorial.png")
tutorial.x = display.contentCenterX + posi/1.5
tutorial.y = display.actualContentHeight * 0.85
tutorial.xScale = 0.06
tutorial.yScale = 0.06

local login = display.newText (sceneGroup,"LOGIN" ,30, 40, "pulsanti/scritta.otf",15)
    login:setFillColor(0, 0, 0)

local titolo = display.newText (sceneGroup,"MERCENARY" ,display.contentCenterX+150, display.actualContentHeight *0.3, "pulsanti/scritta.otf",50)
local titolo2 = display.newText (sceneGroup,"PENGUIN",display.contentCenterX-150, display.actualContentHeight *0.4, "pulsanti/scritta.otf",50)
    titolo:setFillColor(0.2, 0.2, 1)
    titolo2:setFillColor(0.3, 0.5, 1)

transition.to( titolo, { time=1000,x = display.contentCenterX-50, y = display.actualContentHeight *0.3, effect="crossFade" } )
transition.to( titolo2, { time=1000,x = display.contentCenterX+130, y = display.actualContentHeight *0.4, effect="crossFade" } )
  

play:addEventListener( "tap", gotoGame )
score:addEventListener( "tap", gotoHighScores )
tutorial:addEventListener( "tap", gotoTutorial )
credits:addEventListener("tap",gotoCredits )


   

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
  display.remove(animation)
	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		
          composer.removeScene( "menu" )

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
