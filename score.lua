
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

--inizializziamo le variabili

local json = require ("json")
--json è un formato conveniente da usare per lavorare con i dati
--per ora impariamo i due comandi--> json.encode () e json.decode () 
-- consentono rispettivamente di prendere una tabella Lua, archiviare (codificare) i suoi contenuti 
--in un formato noto e successivamente tradurre (decodificare) i dati in una tabella Lua.

local scoresTable = {}
--crea semplicemente una tabella vuota che conterrà eventualmente i punteggi recuperati
--o un elenco aggiornato di punteggi da salvare.

local filePath = system.pathForFile( "scores.json", system.DocumentsDirectory)
--[[
L'ultima riga genera un percorso assoluto per un file JSON (score.json) che useremo per salvare i dieci punteggi più alti. 
Non preoccuparti che questo file non esista ancora - questo comando creerà il file e avvierà un link sotto la variabile filePath.
--]]

local function loadScores()--funzione per caricare la tabella di dati con i best score
 
    local file = io.open( filePath, "r" )
 
    if file then
        local contents = file:read( "*a" )
        io.close( file )
        scoresTable = json.decode( contents )
    end
 
    if ( scoresTable == nil or #scoresTable == 0 ) then
        scoresTable = { 0, 0, 0, 0, 0 }
    end
end

local function saveScores()--salviamo i punteggi nel file json (solo 10)
 
    for i = #scoresTable, 6, -1 do
        table.remove( scoresTable, i )--rimuoviamo tutti i punteggi tranne i primi 10
    end
 
    local file = io.open( filePath, "w" )
 
    if file then
        file:write( json.encode( scoresTable ) )
        io.close( file )
    end
end

local function gotoMenu()--funzione per tornare al menu che abbiamo invocato alla linea 108 tramite il bottone menu
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
	loadScores()--carichiamo la tabella con i punteggi esistenti
	
	--inseriamo il punteggio salvato nell'ultima partita nell'array dei bestScores e lo resettiamo
    table.insert( scoresTable, composer.getVariable( "finalScore" ) )
    composer.setVariable( "finalScore", 0 )
	
	--ordinare i punteggi dal piu al alto al piu piccolo
	local function compare(a,b)
		return a > b
	end
	
	table.sort (scoresTable, compare)
	
	saveScores()
	
	--ora creiamo la scena con gli highScores
	local cielo = display.newImageRect(sceneGroup,"sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
	cielo.x = display.contentCenterX;
	cielo.y = display.contentCenterY;

	local riquadro = display.newImage(sceneGroup,"pulsanti/3.png")
	riquadro.x = display.contentCenterX*1.08;
	riquadro.y = display.contentCenterY;
	riquadro.xScale = 0.26
	riquadro.yScale = 0.24

	local pallino = display.newImage(sceneGroup,"pulsanti/12.png")
    pallino.x = display.contentCenterX * 1.7
	pallino.y = display.contentCenterY * 0.5
	pallino.xScale = 0.11
	pallino.yScale = 0.11

	local coppa = display.newImage(sceneGroup,"pulsanti/coppa.png")
    coppa.x = display.contentCenterX * 1.7
	coppa.y = display.contentCenterY * 0.49
	coppa.xScale = 0.06
	coppa.yScale = 0.06

	local freccia = display.newImage(sceneGroup,"pulsanti/16.png")
    freccia.x = display.contentCenterX / 5.5
	freccia.y = display.contentCenterY * 0.4
	freccia.xScale = 0.1
	freccia.yScale = 0.1
	local menuTesto = display.newText(sceneGroup,"menu", freccia.x+3, freccia.y-2, "pulsanti/scritta.otf", 10 )

	freccia:addEventListener( "tap", gotoMenu )

     
    local highScoresHeader = display.newText(sceneGroup,"High Scores", display.contentCenterX, 100, "pulsanti/scritta.otf", 30 )
	
	for i = 1, 5 do
        if ( scoresTable[i] ) then
            local yPos = 110 + ( i * 25 )
			
			local rankNum = display.newText( sceneGroup, i .. ")", display.contentCenterX-75, yPos, "pulsanti/Clafoutis Regular.ttf", 20 )
            rankNum:setFillColor( 0.2, 0.4, 0.8 )
            rankNum.anchorX = 1
 
            local thisScore = display.newText( sceneGroup, scoresTable[i], display.contentCenterX-50, yPos, "pulsanti/Clafoutis Regular.ttf", 20)
            thisScore.text = string.format( "%0.1f",scoresTable[i])
            thisScore.anchorX = 0
 
        end--questo ciclo for serve per visualizare e posizionare i dieci punteggi piu alti nel formato:
					--   i) punteggio
    end
	
	--creiamo il bottone menu per tornare al menu
	--local menuButton = display.newText( sceneGroup, "Menu", display.contentCenterX, 810, native.systemFont, 44 )
    --menuButton:setFillColor( 0.75, 0.78, 1 )
    --menuButton:addEventListener( "tap", gotoMenu )
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		composer.removeScene( "highscores" ) --per nascondere la scena highScores

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
