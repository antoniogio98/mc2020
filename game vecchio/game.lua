

local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


local physics = require( "physics" )
physics.start()
physics.setGravity( 0, 0 )
--physics.setDrawMode( "hybrid" )

-- Initialize variables
local lives = 3
local score = 0
local died = false
 
local livesText
local scoreText


local sfondoGroup  --sfondo
local mainGroup --pingu
local uiGroup    --tasti

local suonoSparo
local suonoSalto
local suonoMusica
local suonoDolore
local suonoPalla

local posi =  display.actualContentWidth / 2

local datiFoglioCorsa = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

	width = 451,--lunghezza del signolo frame
	height = 442,--altezza del singolo frame
	numFrames = 15,
	sheetContentWidth = 2255,--lunghezza totale function
	sheetContentheight = 1326,--idem
}
local datiFoglioSalto = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

	width = 446,--lunghezza del signolo frame
	height = 680,--altezza del singolo frame
	numFrames = 24,
	sheetContentWidth = 1347,--lunghezza totale function
	sheetContentheight = 1230,--idem
}
local datiFoglioGiu = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 652,--lunghezza del signolo frame
  height = 477,--altezza del singolo frame
  numFrames = 24,
  sheetContentWidth = 3912,--lunghezza totale function
  sheetContentheight = 1908,--idem
}

local datiFoglioSu = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 652,--lunghezza del signolo frame
  height = 514,--altezza del singolo frame
  numFrames = 24,
  sheetContentWidth = 3912,--lunghezza totale function
  sheetContentheight = 2056,--idem
}

local datiFoglioDolore = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 449,--lunghezza del signolo frame
  height = 410,--altezza del singolo frame
  numFrames = 9,
  sheetContentWidth = 2676,--lunghezza totale function
  sheetContentheight = 2720,--idem
}
local datiFoglioMorte = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 750,--lunghezza del signolo frame
  height = 524,--altezza del singolo frame
  numFrames = 32,
  sheetContentWidth = 6000,--lunghezza totale function
  sheetContentheight = 2096,--idem
}

local datiFoglioSparo = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 465,--lunghezza del signolo frame
  height = 442,--altezza del singolo frame
  numFrames = 15,
  sheetContentWidth = 2325,--lunghezza totale function
  sheetContentheight = 1326,--idem
}

local datiFoglioFiamma = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 155,--lunghezza del signolo frame
  height = 158,--altezza del singolo frame
  numFrames = 9,
  sheetContentWidth = 465,--lunghezza totale function
  sheetContentheight = 474,--idem
}

local datiFoglioProiettile = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 110,--lunghezza del signolo frame
  height = 45,--altezza del singolo frame
  numFrames = 9,
  sheetContentWidth = 330,--lunghezza totale function
  sheetContentheight = 135,--idem
}

local datiFoglioBossolo = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 94,--lunghezza del signolo frame
  height = 382,--altezza del singolo frame
  numFrames = 36,
  sheetContentWidth = 654,--lunghezza totale function
  sheetContentheight = 2292,--idem
}

local datiFoglio4 = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 44,--lunghezza del signolo frame
  height = 26,--altezza del singolo frame
  numFrames = 14,
  sheetContentWidth = 616,--lunghezza totale function
  sheetContentheight = 26--idem
}

local datiFoglio5 = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 352/8,--lunghezza del signolo frame
  height = 26,--altezza del singolo frame
  numFrames = 8,
  sheetContentWidth = 352,--lunghezza totale function
  sheetContentheight = 26--idem
}

local datiFoglioGolem = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 900,--lunghezza del signolo frame
  height = 900,--altezza del singolo frame
  numFrames = 12,
  sheetContentWidth = 2702,--lunghezza totale function
  sheetContentheight = 3603--idem
}

local datiFoglioGolemMorte = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 960,--lunghezza del signolo frame
  height = 960,--altezza del singolo frame
  numFrames = 16,
  sheetContentWidth = 3840,--lunghezza totale function
  sheetContentheight = 3840--idem
} 

local datiFoglioFuoco= {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 26,--lunghezza del signolo frame
  height = 10,--altezza del singolo frame
  numFrames = 60,
  sheetContentWidth = 156,--lunghezza totale function
  sheetContentheight = 100--idem
} 
------------------------------------------------------------------------------------------------------------------------------------------
local datiCorsa = {
	{ 
		name = "pinguinoRun",
	 	start = 1,
		count = 15,
		time = 700,
		--loopCount = 0,
		loopDirection = "ended"
		
	}

}

local datiSalto = {
	{ 
		name = "jump",
	 	start = 1,
		count = 24,
		time = 900,
		loopCount = 1,
		loopDirection = "ended",
		fine = false
	}

}
local datiGiu = {
  { 
    name = "giu",
    start = 1,
    count = 24,
    time = 1200,
    loopCount = 1,
    loopDirection = "ended",
    --fine = false
  }

}
local datiSu = {
  { 
    name = "su",
    start = 1,
    count = 24,
    time = 900,
    loopCount = 1,
    loopDirection = "ended",
   fine = false
  }

}

local datiDolore = {
  { 
    name = "dolore",
    start = 1,
    count = 9,
    time = 700,
    loopCount = 1,
    loopDirection = "ended",
    fine = false
  }

}

local datiMorte = {
  { 
    name = "morte",
    start = 1,
    count = 32,
    time = 800,
    loopCount = 1,
    loopDirection = "ended",
   -- fine = false
  }

}
local datiSparo = {
  { 
    name = "sparo",
    start = 1,
    count = 15,
    time = 900,
    loopCount = 1,
    loopDirection = "ended"
    --fine = false
  }

}

local datiFiamma = {
  { 
    name = "fiamma",
    start = 1,
    count = 9,
    time = 200,
    loopCount = 1,
    loopDirection = "ended"
    --fine = false
  }
}

local datiProiettile = {
  { 
    name = "proiettile",
    start = 1,
    count = 9,
    time = 900,
    loopDirection = "ended"
    --fine = false
  }
}

local datiBossolo = {
  { 
    name = "bossolo",
    start = 1,
    count = 36,
    time = 900,
    loopCount = 1,
    loopDirection = "ended"
    --fine = false
  }
}

local datiTarta = {
  { 
    name = "tarta",
    start = 1,
    count = 14,
    time = 500,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}

local datiTarta2 = {
  { 
    name = "tarta2",
    start = 1,
    count = 8,
    time = 600,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}

local datiGolem = {
  { 
    name = "golem corsa",
    start = 1,
    count = 12,
    time = 1000,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}

local datiGolemMorte = {
  { 
    name = "golem morte",
    start = 1,
    count = 16,
    time = 700,
    loopCount = 1,
    loopDirection = "ended"
    
  }

}
local datiFuoco = {
  { 
    name = "fuoco",
    start = 1,
    count = 60,
    time = 900,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}
------------------------------------------------MUOVI SFONDO--------------------------------------------------------------------------------------

local lungh = display.contentWidth
display.setStatusBar(display.HiddenStatusBar)


function muovi (self, event)

  if (self.x ~= nil) then
     if self.x < -lungh/2 then
      self.x = display.contentCenterX+lungh-7
     else 
      self.x = self.x - self.speed
     end
  end

end

--aggiorna il testo in tempo reale
 local function updateText()
    livesText.text = "Lives: " .. lives
    scoreText.text = "Score: " .. score
end
-------------------------------------------------------CORSA-----------------------------------------------------------------------------

local pinguinoRun = graphics.newImageSheet ("move/run.png", datiFoglioCorsa)
local animation = display.newSprite( pinguinoRun, datiCorsa )
physics.addBody( animation, { radius=30, isSensor=true } ) 
animation.myName = "pinguinoRun"

animation.x = -40
animation.y = display.contentCenterX*0.88;
animation.xScale = 0.17
animation.yScale = 0.17
animation:play()

local function camminataInizio(  )
   audio.play( suonoMusica )
   audio.setVolume( 0.5, suonoMusica)
	 transition.to( animation, { time=1200,x = 120.5, y = animation.y} )
end

timer.performWithDelay(2000,camminataInizio)


------------------------------------SALTO-----------------------------------------------------------------------------------------------------------------------
 

 local pinguinoJump = graphics.newImageSheet ("move/jump.png", datiFoglioSalto)
 local animation2 = display.newSprite (pinguinoJump, datiSalto)
 animation2.x = display.contentWidth / 4
 animation2.y = animation.y*0.9;
 animation2.xScale = 0.17
 animation2.yScale = 0.17
 animation2.alpha = 0;

  local function jump()
      audio.play( suonoSalto )
      animation.alpha=0;
      animation2.alpha=1;
      animation2:play()
      
    transition.to( animation2, { time=800,x = animation.x, y = animation.y/1.3,onComplete = function() 
            animation2.x = display.contentWidth / 4
            animation2.y = animation.y*0.9; end} )
      animation.isBodyActive = false

    local function avviaSalto()
      fine = true;
          if (fine) then
            animation.y = display.contentCenterX*0.88;
            animation.alpha = 1;
            animation2.alpha = 0;
            animation.isBodyActive = true
        end
    end

    timer.performWithDelay( 900, avviaSalto )
   

end


 animation2:addEventListener("tap", jump)

-----------------------------------------------GIU/SU-----------------------------------------------------------------------------------------------------------
local pinguinoGiu = graphics.newImageSheet ("move/giu.png", datiFoglioGiu)
 local giuAnimation = display.newSprite (pinguinoGiu, datiGiu)
 giuAnimation.x = display.contentWidth / 4
 giuAnimation.y = animation.y;
 giuAnimation.xScale = 0.17
 giuAnimation.yScale = 0.17
 giuAnimation.alpha = 0;

 local pinguinoSu = graphics.newImageSheet ("move/su.png", datiFoglioSu)
 local suAnimation = display.newSprite (pinguinoSu, datiSu)
 suAnimation.x = display.contentWidth / 4
 suAnimation.y = animation.y;
 suAnimation.xScale = 0.17
 suAnimation.yScale = 0.17
 suAnimation.alpha = 0;



local function giuSu()
       giuAnimation.alpha = 1;
       animation.alpha=0;
       giuAnimation:play()
    
      suAnimation.alpha = 1;
      giuAnimation.alpha = 0;
      suAnimation:play()
   --  animation.isBodyActive = false

    local function torna()
      fine = true;
          if (fine) then
          -- animation.y = display.contentCenterX*0.88;
            suAnimation.alpha = 0;
            animation.alpha = 1;
         --   animation.isBodyActive = true


        end
    end

    timer.performWithDelay( 1000, torna )
   

end


 giuAnimation:addEventListener("tap", giuSu)
 suAnimation:addEventListener("tap", giuSu)


------------------------------------DOLORE-----------------------------------------------------------------------------------------------------------------------
 local pinguinoDolore = graphics.newImageSheet ("move/dolore.png", datiFoglioDolore)
 local doloreAnimation = display.newSprite (pinguinoDolore, datiDolore)
 doloreAnimation.x = display.contentWidth / 4
 doloreAnimation.y = animation.y;
 doloreAnimation.xScale = 0.17
 doloreAnimation.yScale = 0.17
 doloreAnimation.alpha = 0;

------------------------------------MORTE-----------------------------------------------------------------------------------------------------------------------
 local pinguinoMorte= graphics.newImageSheet ("move/morte.png", datiFoglioMorte)
 local morteAnimation = display.newSprite (pinguinoMorte, datiMorte)
 morteAnimation.x = display.contentWidth / 4
 morteAnimation.y = animation.y;
 morteAnimation.xScale = 0.17
 morteAnimation.yScale = 0.17
 morteAnimation.alpha = 0;

------------------------------------SPARO-----------------------------------------------------------------------------------------------------------------------

local pinguinoShot = graphics.newImageSheet ("move/shot.png", datiFoglioSparo)
local animation3 = display.newSprite(pinguinoShot, datiSparo)

 animation3.x = display.contentWidth / 4;
 animation3.y = animation.y;
 animation3.xScale = 0.17;
 animation3.yScale = 0.17;
 animation3.alpha = 0;

local fiamma = graphics.newImageSheet ("move/fiamma.png", datiFoglioFiamma)
local fiammaAnimation = display.newSprite(fiamma, datiFiamma)

 fiammaAnimation.x = display.contentWidth /3;
 fiammaAnimation.y = animation.y*0.99;
 fiammaAnimation.xScale = 0.17;
 fiammaAnimation.yScale = 0.17;
 fiammaAnimation.alpha = 0;

local proiettile = graphics.newImageSheet ("move/proiettile.png", datiFoglioProiettile)
local proiettileAnimation = display.newSprite(proiettile, datiProiettile)
  physics.addBody( proiettileAnimation, { radius=30, isSensor=true } ) 
  proiettileAnimation.myName = "proiettile"

 proiettileAnimation.x = display.contentWidth /3;
 proiettileAnimation.y = animation.y*0.99;
 proiettileAnimation.xScale = 0.32;
 proiettileAnimation.yScale = 0.32;
 proiettileAnimation.alpha = 0;


local bossolo = graphics.newImageSheet ("move/bossolo.png", datiFoglioBossolo)
local bossoloAnimation = display.newSprite(bossolo, datiBossolo)

 bossoloAnimation.x = display.contentWidth /2.98;
 bossoloAnimation.y = animation.y*0.99;
 bossoloAnimation.xScale = 0.27;
 bossoloAnimation.yScale = 0.27;
 bossoloAnimation.alpha = 0;

local function creaSparo()


    local proiettileAnimation = display.newSprite(proiettile, datiProiettile)
    physics.addBody( proiettileAnimation, { radius=30, isSensor=true } ) 
    proiettileAnimation.myName = "proiettile"

    proiettileAnimation.x = display.contentWidth /3;
    proiettileAnimation.y = animation.y*0.99;
    proiettileAnimation.xScale = 0.32;
    proiettileAnimation.yScale = 0.32;
    proiettileAnimation.alpha = 0;
  
    transition.to( proiettileAnimation, { time=1000,x = display.actualContentWidth*1.5, y = animation.y ,onComplete = function() display.remove( proiettileAnimation ) end
    })

    proiettileAnimation:play()
    proiettileAnimation.alpha = 0.8;

end


local function shot()

	audio.play( suonoSparo )
	animation3.alpha=1;
    animation.alpha=0;
    animation3:play()
    fiammaAnimation:play()
    fiammaAnimation.alpha = 0.6;

    creaSparo()

    bossoloAnimation : play()
    bossoloAnimation.alpha = 1;

    local function avviaSparo()

      fine = true;
          if (fine) then
          animation.alpha = 1;
          animation3.alpha = 0;
          fiammaAnimation.alpha = 0;
          proiettileAnimation.alpha = 0;
          bossoloAnimation.alpha = 0;

    end
end

    timer.performWithDelay( 800, avviaSparo )
end

	animation3:addEventListener("tap", shot)

------------------------------------TARTARUGA-----------------------------------------------------------------------------------------------------------------------

local animaTarta = graphics.newImageSheet ("nemici/tarta.png", datiFoglio4)
local tartaruga = display.newSprite( animaTarta, datiTarta )

physics.addBody( tartaruga, { radius=30, isSensor=true } ) 
tartaruga.myName = "tarta"
tartaruga.x = display.contentCenterX*2.5
tartaruga.y = display.contentCenterX*0.98;
tartaruga.xScale = 0.8
tartaruga.yScale = 0.8
tartaruga:play()


------------------------------------GOLEM-----------------------------------------------------------------------------------------------------------------------

local golem = graphics.newImageSheet ("nemici/golem.png", datiFoglioGolem)
local animaGolemCorsa = display.newSprite(golem, datiGolem)
physics.addBody( animaGolemCorsa, { radius=30, isSensor=true } ) 
animaGolemCorsa.myName = "golem"

 animaGolemCorsa.x = display.contentCenterX *2.4;
 animaGolemCorsa.y = display.contentCenterX*0.85;
 animaGolemCorsa.xScale = 0.13;
 animaGolemCorsa.yScale = 0.13;
 animaGolemCorsa:play()
 animaGolemCorsa.myName = "golem"


local golemMorte = graphics.newImageSheet ("nemici/morte.png", datiFoglioGolemMorte)
local animaGolemMorte = display.newSprite(golemMorte, datiGolemMorte)
 animaGolemMorte.xScale = 0.13;
 animaGolemMorte.yScale = 0.13;
 animaGolemMorte.alpha = 0;


---------------------------------------------------------------FUOCO-----------------------------------------------------------------------------

local fuoco = graphics.newImageSheet ("nemici/fuoco.png", datiFoglioFuoco)
local fuocoAnimation = display.newSprite(fuoco, datiFuoco)
physics.addBody( fuocoAnimation, { radius=30, isSensor=true } ) 
fuocoAnimation.myName = "fuoco"

 fuocoAnimation.x = display.contentCenterX *4;
 fuocoAnimation.y = display.contentCenterX*0.8
 fuocoAnimation.xScale = 3.5;
 fuocoAnimation.yScale = 3.5;
 fuocoAnimation.alpha = 0;

 ----------------------------------------CAMMINATA-------------------------------------------------------------------------------------------------------


local function camminataNemici(  )
if(lives>0) then
local num = math.random(3)
    if (num==1) then
       local animaTarta2 = graphics.newImageSheet ("nemici/tarta2.png", datiFoglio5)
       local tartaruga = display.newSprite( animaTarta2, datiTarta2 )
       physics.addBody( tartaruga, { radius=30, isSensor=true } ) 
        tartaruga.myName = "tarta"
      tartaruga.x = display.contentCenterX*2.5
      tartaruga.y = display.contentCenterX*0.98;
      tartaruga.xScale = 0.8
      tartaruga.yScale = 0.8
      tartaruga:play()
      transition.to( tartaruga, { time=3000,x = -120.5, y = tartaruga.y, onComplete = function() display.remove(tartaruga)end} )

  elseif (num==2) then
       local golem = graphics.newImageSheet ("nemici/golem.png", datiFoglioGolem)
       local animaGolemCorsa = display.newSprite(golem, datiGolem)
       physics.addBody( animaGolemCorsa, { radius=30, isSensor=true } ) 
       animaGolemCorsa.myName = "golem"
       animaGolemCorsa.x = display.contentCenterX *2.4;
       animaGolemCorsa.y = display.contentCenterX*0.85;
       animaGolemCorsa.xScale = 0.13;
       animaGolemCorsa.yScale = 0.13;
       animaGolemCorsa:play()
       transition.to( animaGolemCorsa, { time=2000,x = -35, y = animaGolemCorsa.y ,onComplete = function() display.remove(animaGolemCorsa)end} )

      else 
        local fuoco = graphics.newImageSheet ("nemici/fuoco.png", datiFoglioFuoco)
        local fuocoAnimation = display.newSprite(fuoco, datiFuoco)
        physics.addBody( fuocoAnimation, { radius=30, isSensor=true } ) 
        fuocoAnimation.myName = "fuoco"
        fuocoAnimation.x = display.contentCenterX *2.4;
        fuocoAnimation.y = display.contentCenterX*0.85;
        fuocoAnimation.xScale = 4;
        fuocoAnimation.yScale = 4;
        fuocoAnimation.alpha = 1;
        fuocoAnimation:play()
        audio.play( suonoPalla )
        transition.to( fuocoAnimation, { time=3000,x = -200, y = fuocoAnimation.y ,onComplete = function() display.remove(fuocoAnimation)end} )
  
      end
    end
  end
      timer.performWithDelay( math.random(4000, 15000), camminataNemici , 10)


-------------------------------------------------------COLLISIONI---------------------------------------------------------------------

--fine torna al menu
 local function endGame()

  composer.setVariable( "finalScore", score )
    composer.gotoScene( "score", { time=500, effect="crossFade" } )
end


local function onCollision(event )
 
    if ( event.phase == "began" ) then
 
        local obj1 = event.object1
        local obj2 = event.object2
 
        if ( obj1.myName == "proiettile" and obj2.myName == "golem") then
              
               animaGolemMorte.alpha = 1;
               animaGolemMorte.x =  obj2.x
               animaGolemMorte.y =  obj2.y*1.06
               animaGolemMorte:play()
               display.remove( obj2 )

          elseif ( obj1.myName == "golem" and obj2.myName == "proiettile" ) then
            
                animaGolemMorte.alpha = 1;
                animaGolemMorte.x =  obj2.x
                animaGolemMorte.y =  obj2.y*1.06
                animaGolemMorte:play()
                display.remove( obj1 ) 
        
   end
  
  end 
end


local function onCollision2(event )
 display.remove( proiettileAnimation )
 if ( event.phase == "began" ) then
 
        local obj1 = event.object1
        local obj2 = event.object2

if (( obj1.myName == "pinguinoRun" and obj2.myName == "golem") or
          (obj1.myName == "golem" and obj2.myName == "pinguinoRun" ) ) then 
         
          animation.alpha = 0;
          doloreAnimation.alpha = 1;
          doloreAnimation:play();
         audio.play( suonoDolore )

            local function tornaCorsa()
               doloreAnimation.alpha = 0;
               animation.alpha = 1;

                lives = lives - 1
                livesText.text = "Lives: " .. lives

               if ( lives == 0) then
                     -- Increase score
                    morteAnimation.alpha = 1;
                    morteAnimation:play();
                    score =  newScore
                    scoreText.text = "Score: " .. score

                   timer.performWithDelay( 500, endGame ) 

                       display.remove(tartaruga)
                       display.remove(animaGolemCorsa)
                       display.remove(animation) 
                       display.remove(fuocoAnimation) 

                 end
                 
             end

    timer.performWithDelay( 700, tornaCorsa )
      end
    end
end

local function onCollisionTarta(event )
 
    if ( event.phase == "began" ) then
 
        local obj1 = event.object1
        local obj2 = event.object2

        if ( (obj1.myName == "pinguinoRun" and obj2.myName == "tarta" )  or
             ( obj1.myName == "tarta" and obj2.myName == "pinguinoRun" )) then
            
             animation.alpha = 0;
             doloreAnimation.alpha = 1;
             
             doloreAnimation:play();
             audio.play( suonoDolore )

            local function tornaCorsa()
               doloreAnimation.alpha = 0;
               animation.alpha = 1;

                lives = lives - 1
                livesText.text = "Lives: " .. lives

               if ( lives == 0 ) then
                       morteAnimation.alpha = 1;
                       morteAnimation:play();
                     -- Increase score
                    score =  newScore
                    scoreText.text = "Score: " .. score

                   timer.performWithDelay( 500, endGame ) 

                       display.remove(tartaruga)
                       display.remove(animaGolemCorsa)
                       display.remove(animation) 
                       display.remove(fuocoAnimation) 

                 end
                 
             end

    timer.performWithDelay( 700, tornaCorsa )
      end
    end
end





local function onCollisionFuoco(event )
 
    if ( event.phase == "began" ) then
 
        local obj1 = event.object1
        local obj2 = event.object2

        if ( (obj1.myName == "pinguinoRun" and obj2.myName == "fuoco" )  or
             ( obj1.myName == "fuoco" and obj2.myName == "pinguinoRun" )) then
            
             animation.alpha = 0;
             doloreAnimation.alpha = 1;
             
             doloreAnimation:play();
             audio.play( suonoDolore )

            local function tornaCorsa()
               doloreAnimation.alpha = 0;
               animation.alpha = 1;

                lives = lives - 1
                livesText.text = "Lives: " .. lives

               if ( lives == 0 ) then
                       morteAnimation.alpha = 1;
                       morteAnimation:play();
                     -- Increase score
                    score =  newScore
                    scoreText.text = "Score: " .. score

                   timer.performWithDelay( 500, endGame ) 

                       display.remove(tartaruga)
                       display.remove(animaGolemCorsa)
                       display.remove(animation) 
                       display.remove(fuocoAnimation) 

                 end
                 
             end

    timer.performWithDelay( 700, tornaCorsa )
      end
    end
end



 
proiettileAnimation.collision = onCollision
proiettileAnimation:addEventListener( "collision" )

animaGolemCorsa.collision = onCollision
animaGolemCorsa:addEventListener( "collision" )


animaGolemCorsa.collision = onCollision2
animaGolemCorsa:addEventListener( "collision" )

animation.collision = onCollision2
animation:addEventListener( "collision" )
 
animation.collision = onCollisionTarta
animation:addEventListener( "collision" )

tartaruga.collision = onCollisionTarta
tartaruga:addEventListener( "collision" )

 
animation.collision = onCollisionFuoco
animation:addEventListener( "collision" )

fuocoAnimation.collision = onCollisionFuoco
fuocoAnimation:addEventListener( "collision" )


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen


    -- Set up display groups
    sfondoGroup = display.newGroup()  
    sceneGroup:insert( sfondoGroup )  
 
    mainGroup = display.newGroup()  
    sceneGroup:insert( mainGroup )  
 
    uiGroup = display.newGroup()   
    sceneGroup:insert( uiGroup )    



local salto = display.newRect( uiGroup,400, 400, 400, 200)
salto.x = display.contentCenterX -200;
salto.y = display.contentCenterY-40;
salto.alpha = 0.5;
salto:addEventListener("tap",jump)

local giu = display.newRect( uiGroup, 400, 400, 400, 200)
giu.x = display.contentCenterX -200;
giu.y = display.contentCenterY+180;
giu.alpha = 0.9;
giu:addEventListener("tap",giuSu)

local spara = display.newImage(uiGroup, "pulsanti/spara.png")
spara.x = display.contentCenterX *1.8
spara.y = display.actualContentHeight * 1.0
spara.xScale = 0.06
spara.yScale = 0.06
spara.alpha = 0.8
spara:addEventListener("tap",shot)

local cielo = display.newImageRect(sfondoGroup, "sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
cielo.x = display.contentCenterX;
cielo.y = display.contentCenterY;
cielo.speed = 0.8

local cielo2 = display.newImageRect(sfondoGroup,"sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
cielo2.x = display.contentCenterX*3;
cielo2.y = display.contentCenterY;
cielo2.speed = 0.8

local montagne = display.newImageRect(sfondoGroup,"sfondi/BG.png",display.actualContentWidth, display.actualContentHeight)
montagne.x = display.contentCenterX;
montagne.y = display.contentCenterY/0.95;
montagne.speed = 1

local montagne2 = display.newImageRect(sfondoGroup,"sfondi/BG.png",display.actualContentWidth, display.actualContentHeight)
montagne2.x = display.contentCenterX*3;
montagne2.y = display.contentCenterY/0.95;
montagne2.speed = 1

local alberi= display.newImageRect(sfondoGroup,"sfondi/Middle.png",display.actualContentWidth, display.actualContentHeight)
alberi.x = display.contentCenterX;
alberi.y = display.contentCenterY/0.88; 
alberi.speed = 2

local alberi2= display.newImageRect(sfondoGroup,"sfondi/Middle.png",display.actualContentWidth, display.actualContentHeight)
alberi2.x = display.contentCenterX*3;
alberi2.y = display.contentCenterY/0.88; 
alberi2.speed = 2

local pavi1 = display.newImageRect(sfondoGroup,"sfondi/terra.png",display.actualContentWidth+3, display.actualContentHeight)
pavi1.x = display.contentCenterX;
pavi1.y = display.contentCenterY*1.1;
pavi1.speed = 3

local pavi2 = display.newImageRect(sfondoGroup,"sfondi/terra.png",display.actualContentWidth+3, display.actualContentHeight)
pavi2.x = display.contentCenterX*3;
pavi2.y = display.contentCenterY*1.1;
pavi2.speed = 3

suonoSparo = audio.loadSound( "suoni/suonoSparo2.wav" )

suonoSalto = audio.loadSound ("suoni/suonoSalto.wav")

suonoMusica = audio.loadStream ("suoni/musica.wav")

suonoPalla = audio.loadSound("suoni/suonoPalla.wav")

suonoDolore = audio.loadSound ("suoni/dolore.wav")
 
cielo.enterFrame = muovi
Runtime:addEventListener("enterFrame", cielo)
cielo2.enterFrame = muovi
Runtime:addEventListener("enterFrame", cielo2)

montagne.enterFrame = muovi
Runtime:addEventListener("enterFrame", montagne)
montagne2.enterFrame = muovi
Runtime:addEventListener("enterFrame", montagne2)

alberi.enterFrame = muovi
Runtime:addEventListener("enterFrame", alberi)
alberi2.enterFrame = muovi
Runtime:addEventListener("enterFrame", alberi2)

pavi1.enterFrame = muovi
Runtime:addEventListener("enterFrame", pavi1)
pavi2.enterFrame = muovi
Runtime:addEventListener("enterFrame", pavi2)


-- Display lives and score



local sound = display.newImage(uiGroup,"pulsanti/sound.png")
sound.x = display.contentCenterX + posi/1.2
sound.y = display.actualContentHeight * 0.25
sound.xScale = 0.06
sound.yScale = 0.06

local noSound = display.newImage(uiGroup,"pulsanti/noSound.png")
noSound.x = display.contentCenterX + posi/1.2
noSound.y = display.actualContentHeight * 0.25
noSound.xScale = 0.06
noSound.yScale = 0.06
noSound.alpha = 0

 local function soundOff( )
      if(noSound.alpha == 0) then
        audio.setVolume( 0, suonoMusica)
          noSound.alpha = 1;
        sound.alpha = 0;
   end
end
    sound:addEventListener( "tap", soundOff )


 local function soundOn( ) 
   if(noSound.alpha == 1) then
         audio.setVolume( 0.5, suonoMusica)
         noSound.alpha = 0;
         sound.alpha = 1;
 end
end


 noSound:addEventListener( "tap", soundOn )


local life = display.newImage(uiGroup, "pulsanti/life.png")
life.x = display.contentCenterX /4
life.y = display.actualContentHeight * 0.25
life.xScale = 0.06
life.yScale = 0.06

local metri = display.newImage(uiGroup, "pulsanti/metri.png")
metri.x = display.contentCenterX /2
metri.y = display.actualContentHeight * 0.25
metri.xScale = 0.08
metri.yScale = 0.08

livesText = display.newText( uiGroup, "Lives: " ..lives, display.contentCenterX /3.4, display.actualContentHeight * 0.25, "pulsanti/scritta.otf", 7.5 )
scoreText = display.newText( uiGroup, score, display.contentCenterX /2, display.actualContentHeight * 0.23, "pulsanti/Clafoutis Regular.ttf", 8 )
local mText = display.newText( uiGroup, "Mt" , display.contentCenterX /2, display.actualContentHeight * 0.26, "pulsanti/Clafoutis Regular.ttf", 7 )
livesText:setFillColor( 1, 1, 1 )
scoreText:setFillColor(1, 1, 1)
mText:setFillColor(1, 1, 1)

 
local function lerp( v0, v1, t )
    return v0 + t * (v1 - v0)
end
 
local function incrementScore( target, amount, duration, startValue )
 
    newScore = startValue or 0
    local passes = (duration/900) * display.fps
    local increment = lerp( 0, amount, 1/passes )
 
    local count = 0
    local function updateText()
        if ( count <= passes ) then
            newScore = newScore + increment
            target.text = string.format( "%01d", newScore )
            count = count + 1
        else
            Runtime:removeEventListener( "enterFrame", updateText )
            target.text = string.format( "%01d", amount + (startValue or 0) )
        end
    end
 
    Runtime:addEventListener( "enterFrame", updateText )
end


incrementScore( scoreText, 750800, 400000000)


end


----------------------------------------------------------------------------------------------------------------------------------------------------------
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
        physics.start()
        Runtime:addEventListener( "collision", onCollision )
        Runtime:addEventListener( "collision", onCollision2 )
        Runtime:addEventListener( "collision", onCollisionTarta )
        Runtime:addEventListener( "collision", onCollisionFuoco )
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
       
	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

    Runtime:removeEventListener( "collision", onCollision )
    Runtime:removeEventListener( "collision", onCollision2 )
    Runtime:removeEventListener( "collision", onCollisionTarta )
    Runtime:removeEventListener( "collision", onCollisionFuoco )
    physics.pause()
    audio.pause( suonoMusica )
    composer.removeScene( "game" )
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
